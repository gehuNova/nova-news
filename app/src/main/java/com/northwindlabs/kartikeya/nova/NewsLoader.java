package com.northwindlabs.kartikeya.nova;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import java.util.List;

public class NewsLoader extends AsyncTaskLoader<List<News>> {

    private static final String LOG_TAG = "NewsLoader";

    private String url = null;

    public NewsLoader(Context context, String mUrl) {
        super(context);
        Log.i(LOG_TAG, "default constructor called.");
        url = mUrl;
    }

    @Override
    protected void onStartLoading() {
        Log.i(LOG_TAG, "onStartLoading called.");
        forceLoad();
    }

    @Override
    public List<News> loadInBackground() {
        Log.i(LOG_TAG, "loadInBackground called.");
        // Don't perform the request if there are no URLs
        if (url == null) {
            return null;
        }
        List<News> newsList = NewsQueryUtils.fetchRecipeData(url);
        return newsList;
    }

}
