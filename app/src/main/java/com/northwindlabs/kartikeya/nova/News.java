package com.northwindlabs.kartikeya.nova;

public class News {
    private String sourceName;
    private String sourceUrl;
    private String title;
    private String imageUrl;
    private String publishedAt;
    private String content;

    public News(String sourceName, String sourceUrl, String title, String imageUrl, String publishedAt, String content) {
        this.sourceName = sourceName;
        this.sourceUrl = sourceUrl;
        this.title = title;
        this.imageUrl = imageUrl;
        this.publishedAt = publishedAt;
        this.content = content;
    }

    public String getSourceName() {
        return sourceName;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public String getContent() {
        return content;
    }
}
