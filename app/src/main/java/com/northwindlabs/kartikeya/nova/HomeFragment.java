package com.northwindlabs.kartikeya.nova;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;

import com.special.ResideMenu.ResideMenu;

public class HomeFragment extends Fragment {
    private ResideMenu resideMenu;
    private View parentView;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState){
        parentView = inflater.inflate(R.layout.fragment_home, container, false);
        setUpViews();

        return parentView;
    }

    private void setUpViews(){
        MainActivity parentActivity = (MainActivity) getActivity();
        resideMenu = parentActivity.getResideMenu();
    }
}
