package com.northwindlabs.kartikeya.nova;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;

public class MainActivity extends FragmentActivity implements View.OnClickListener{

    private ResideMenu resideMenu;
    private MainActivity mContext;
    private ResideMenuItem itemHome;
    private ResideMenuItem itemCategory;
    private ResideMenuItem itemForYou;
    private ResideMenuItem itemSettings;
    private ResideMenuItem itemAbout;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        setUpMenu();
        if( savedInstanceState == null )
            changeFragment(new HomeFragment());
    }

    private void setUpMenu() {

        // attach to current activity;
        resideMenu = new ResideMenu(this);
        resideMenu.setBackground(R.drawable.menu_background);
        resideMenu.attachToActivity(this);
        //resideMenu.setMenuListener(menuListener);

        //valid scale factor is between 0.0f and 1.0f. leftmenu'width is 150dip.
        resideMenu.setScaleValue(0.6f);

        // create menu items;
        itemHome = new ResideMenuItem(this, R.drawable.ic_action_home, R.string.menu_item_home_title);
        itemCategory = new ResideMenuItem(this, R.drawable.ic_action_category, R.string.menu_item_categories_title);
        itemForYou = new ResideMenuItem(this, R.drawable.ic_action_foryou, R.string.menu_item_for_you_title);
        itemSettings = new ResideMenuItem(this, R.drawable.ic_action_settings, R.string.menu_item_setting_title);
        itemAbout = new ResideMenuItem(this, R.drawable.ic_action_about, R.string.menu_item_about_title);


        itemHome.setOnClickListener(this);
        itemCategory.setOnClickListener(this);
        itemForYou.setOnClickListener(this);
        itemSettings.setOnClickListener(this);
        itemAbout.setOnClickListener(this);

        resideMenu.addMenuItem(itemHome, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemCategory, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemForYou, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemSettings, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemAbout, ResideMenu.DIRECTION_LEFT);

        // Disabling right swipe, we don't need this
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);

        findViewById(R.id.title_bar_left_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return resideMenu.dispatchTouchEvent(ev);
    }

    @Override
    public void onClick(View view) {

        if(view == itemHome){
            changeFragment(new HomeFragment());
        }
        if(view == itemCategory){
            changeFragment(new CategoryFragment());
        }
        if(view == itemForYou){
            changeFragment(new ForYouFragment());
        }
        if(view == itemSettings){
            changeFragment(new SettingsFragment());
        }
        if(view == itemAbout){
            changeFragment(new AboutFragment());
        }


        resideMenu.closeMenu();
    }



    private void changeFragment(Fragment targetFragment){
        resideMenu.clearIgnoredViewList();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    public ResideMenu getResideMenu(){
        return resideMenu;
    }

    private void initView() {
        RecyclerView recyclerView = findViewById(R.id.rv);
        recyclerView.setAdapter(new NewsAdapter(this));
    }
}